@extends('layout.template')

@section('content')

            @if(session('sukses'))
            <div class="alert alert-success mt-2" role="alert">
            {{ session('sukses') }}
            </div>
            @endif
            <div class="row">
                <div class="col-9">
                    {{-- <h1>Sistem Informasi Akademik</h1> --}}
                    <h1>Data Mahasiswa</h1>
                </div>
                <div class="col-3">
                    <a href="/add" class="btn btn-primary mt-3 pull-right">Tambah Data</a>
                </div>
                <table class="table table-hover table-bordered border-dark table-striped">
                    <thead class="table-black">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIM</th>
                            <th>Kelas</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>
                    @foreach($mahasiswa as $p)
                    <tr>
                        <td>{{ $p->id }}</td>
                        <td>{{ $p->nama_mahasiswa }}</td>
                        <td>{{ $p->nim_mahasiswa }}</td>
                        <td>{{ $p->kelas_mahasiswa }}</td>
                        <td>{{ $p->prodi_mahasiswa }}</td>
                        <td>{{ $p->fakultas_mahasiswa }}</td>
                        <td>
                            <a href="/edit/{{ $p->id }}" class="btn btn-warning"><i class=""></i> Edit Data</a>
                            
                            <a href="/delete/{{ $p->id }}" class="btn btn-danger" onclick="return confirm('Apakah yakin ingin dihapus?')"><i class=""></i> Hapus Data</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
@endsection